#ifndef HELPERFNCT_H
#define HELPERFNCT_H

#include <mbed.h>
#include <driveDriver.h>
#include <servoInterface.h>

/**** Some constants ****/
const float   tPerStepZ = 700;      //minimal delay between the steps in µs
const double  zPerStep  = 8.0 * 1.8 * 3.0 / 360.0;  //distance driven by executing one step in cm

/**** Serial connections ****/
extern Serial pc;

// Interrupt when a endstop is hit
InterruptIn endStopZ(PC_4, PullDown);
bool endStopHit = false;

/**** Vacuum ****/
// Pump
DigitalOut  pumpRelais(PC_13, 1);
// Solenoids
DigitalOut  vacuumRelease(PB_7, 1);
// Sensor
AnalogIn    vacSensor(A2);

/**** Pins for the Arm ****/
//Stepper controller
DigitalOut  armZStep(PC_3);
DigitalOut  armZDir(PC_2);
DigitalOut  armZEnable(PC_0, 1);

// With these we detect the team
DigitalIn   teamOrange(PC_9, PullUp);
DigitalIn   teamGreen(PC_8, PullUp); //purple

bool pauseMeasure = false;

const int heightAtom = 25; //mm

void homeZAxis();
void moveZAxisAbsolute(int heightTo);
void moveZAxis(int dist);
void dropAtom();

// Gets the team we're in
// Set it by the team-selector switch
int getTeam() {
  if (teamGreen == 0) {
    return 1;
  }
  else if(teamOrange == 0) {
    return 2;
  }
  else {
    return 3;
  }
}

void switchPump(bool on) {
  pumpRelais = !on;
}

void hitEndStop() {
  if (endStopZ){
    endStopHit = true;
  }
}

void initAll() {
  printf("Program initializing!\n");

  endStopZ.rise(&hitEndStop);
  printf("Interrupts done!\n");

  initAllSensors();

  printf("Servos initializing!\n");
  moveJointLVL3Up(true);
  moveJointAbsolute(90, 0, 500);
  printf("Servos moved to start positions\n");
  homeZAxis();
  moveZAxis(100);
  moveJointLVL3Up(false);
  moveZAxisAbsolute(64);
  moveJointAbsolute(-5, -45, 500);
}

void collect() {
  printf("Collect atom from World-Space\n");
  moveJointLVL3Up(false);
  goToPoint(85, 85, false);
  printf("Pump on\n");
  switchPump(true);
  moveZAxisAbsolute(22);
}

void collectStorage(int index) {
  printf("Collect atom from storage on position %d\n", index);
  moveJointLVL3Up(false);
  moveZAxisAbsolute(60 + heightAtom + index * heightAtom + 20);
  goToPoint(120, -40, false);
  printf("Pump on\n");
  switchPump(true);
  moveZAxisAbsolute(60 + heightAtom + index * heightAtom);
  wait_ms(300);
  moveZAxis(10);
}

void store(int index) {
  printf("Store atom to position %d\n", index);
  moveJointLVL3Up(false);
  moveZAxisAbsolute(60 + heightAtom + 10 + index * heightAtom);
  goToPoint(120, -40, false);
  printf("Drop Atom\n");
  dropAtom();
}

void dropAtom() {
  pumpRelais = 1;
  wait_ms(100);
  vacuumRelease = 0;
  wait_ms(100);
  vacuumRelease = 1;
}

void moveArmZ(unsigned int steps, bool dir) {
  printf("I'll do %d steps on the Arm\n", steps);
  pauseMeasure = true;

  armZDir = dir ? 1 : 0;
  armZEnable = 0;

  endStopHit = false;

  wait_ms(10);

  float a = 0; //for acceleration
  for (unsigned int i = 0; i < steps; i++) {
    
    /*if (endStopZ == 1 && endStopHit) {
      for (int s = 0; s < 30; s++) {
        a = 0;
        a = tPerStepZ * 0.0080 * (30 - s);

        armZStep = 1;
        wait_us(tPerStepZ + a);
        armZStep = 0;
        wait_us(tPerStepZ + a);
      }
      break;
    }*/
    
    a = 0;
    if (i < 100 && i <= steps / 2) {
      a = tPerStepZ * 0.0025 * (100 - i);
    }

    if (steps - i < 100 && i > steps / 2) {
      a = tPerStepZ * 0.0025 * (100 - (steps - i));
    }

    armZStep = 1;
    wait_us(tPerStepZ + a);
    armZStep = 0;
    wait_us(tPerStepZ + a);
  }
  endStopHit = false;
  pauseMeasure = false;
  armZEnable = 1;
}

void homeZAxis(){
  zAxisHeight = zAxisLimitSwitchGroundOffset;
  pauseMeasure = true;

  printf("Arm is homing\n");

  armZDir = 1;
  armZEnable = 0;

  endStopHit = false;

  wait_ms(10);

  float a = 0; //for acceleration
  int i = 0;
  while (true) {
    
    if (endStopZ.read() == 1) {
      for (int s = 0; s < 30; s++) {
        a = 0;
        a = tPerStepZ * 0.0080 * (30 - s);

        armZStep = 1;
        wait_us(tPerStepZ * 2 + a);
        armZStep = 0;
        wait_us(tPerStepZ * 2 + a);
      }
      break;
    }
    
    a = 0;
    if (i < 40) {
      a = tPerStepZ * 0.0075 * (40 - i);
    }

    armZStep = 1;
    wait_us(tPerStepZ + a);
    armZStep = 0;
    wait_us(tPerStepZ + a);
    i++;
  }
  endStopHit = false;
  pauseMeasure = false;
  armZEnable = 1;
}

//moves the ZAxis up or down by dist mm
void moveZAxis(int dist) {
  int distToMove = dist;
  if((zAxisHeight+dist) < zAxisLimitSwitchGroundOffset){
    distToMove = zAxisLimitSwitchGroundOffset-zAxisHeight;
    printf("Request for zAxis movement out of softlimits\n");
  } else if((zAxisHeight+dist) > zAxisMaxGroundOffset){
    distToMove = zAxisMaxGroundOffset-zAxisHeight;
    printf("Request for zAxis movement out of softlimits\n");
  }
  zAxisHeight += distToMove;
  unsigned int stepsToDo = abs(distToMove) / zPerStep;
  moveArmZ(stepsToDo, distToMove < 0);
}

void moveZAxisAbsolute(int heightTo){//moves the suction cup to the height height (in mm) over the ground (automatically corrects for the jointLVL3FacingUp state)
  int distToMove = heightTo-zAxisHeight;
  if(jointLVL3FacingUp){distToMove-=jointLVL3Length;}
  moveZAxis(distToMove);
}

bool searchAtom() {
  bool atomFound = false;

  int normalizedArm = 0;

  do
  {
    
  } while (!atomFound);
  return false;
}

#endif