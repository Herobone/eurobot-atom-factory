#ifndef LIGHT_H
#define LIGHT_H

#include <mbed.h>

PwmOut red(PA_11);
PwmOut green(D12);
PwmOut blue(D13);

void setColor(float r, float g, float b) {
    red.write(r / 255);
    green.write(g / 255);
    blue.write(b / 255);
}

void setViolet() {
    setColor(132, 76, 130);
}

void setOrange() {
    setColor(247, 181, 0);
}

void off() {
    setColor(0,0,0);
}

void setRed() {
    setColor(255,0,0);
}

void setGreen() {
    setColor(0, 255, 0);
}

void setBlue() {
    setColor(0,0,255);
}

#endif