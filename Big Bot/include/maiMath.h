#pragma once

/**** Some constants ****/
const double pi = atan(1.0) * 4;        // Math PI (Why not existing in C++?)

#define sq(x) pow(x, 2)

// Function for calculating mean 
double findMean(int a[], int n) 
{ 
    int sum = 0; 
    for (int i = 0; i < n; i++)  
        sum += a[i]; 
      
    return (double)sum/(double)n; 
} 
  
// Function for calculating median 
double findMedian(int a[], int n) 
{ 
    // First we sort the array 
    sort(a, a+n); 
  
    // check for even case 
    if (n % 2 != 0) 
       return (double)a[n/2]; 
      
    return (double)(a[(n-1)/2] + a[n/2])/2.0; 
} 