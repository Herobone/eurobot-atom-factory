#pragma once

#include <mbed.h>
#include <maiMath.h>
#include <stdbool.h>
#include <driveDriver.h>

int lastArmXOffSet = 0;
bool jointLVL3FacingUp = false;
int zAxisHeight = 0;

int currentAngleJointLVL1 = 0;
int currentAngleJointLVL2 = 0; 

const double jointLVL1Length = 80.0;
const double jointLVL2Length = 44.5;
const double jointLVL2LengthScutionCup = 47.0;
const double jointLVL2LengthSensor = 58.5;
const double jointLVL3Length = 88.5;
const int jointLVL2ToSuctionCupAngleOffset = 19;//in refrence to servo0ToRobot0JointLVL2
const int jointLVL2ToSensorAngleOffset = -41;//in refrence to servo0ToRobot0JointLVL2
const int zAxisLimitSwitchGroundOffset = 22;//This is the distance from the suction-cup to the ground (jointLVL3FacingUp = false) when the zAxisLimitSwitch is barrely activated in mm     !!!!!!!!!!!!!!!!NEEDS ADJUSTMENT!!!!!!!!!!!!!!!!
const int zAxisMaxGroundOffset = 145;//This is the max distance from the suction-cup to the ground (jointLVL3FacingUp = false) in mm     !!!!!!!!!!!!!!!!NEEDS ADJUSTMENT!!!!!!!!!!!!!!!!


/**** Serial connections ****/
extern Serial pc;
Serial servoSerial(A0, A1, 115200);

/**
 * 0 = totaly left
 * 180 = totaly right
 * 90 = straight
*/
void writeMega(int angle) {
  servoSerial.printf("!!m%d\n", angle);
}

void moveJointLVL3Up(bool state){
  if (state == jointLVL3FacingUp) {
    return;
  }
  jointLVL3FacingUp = state;
  servoSerial.printf("!!t%d\n", state ? 1 : 0);
  wait_ms(1220);
}

/**
 * 0 = totaly left
 * 180 = totaly right
 * 90 = straight
*/
void writeElle(int angle) {
  servoSerial.printf("!!e%d\n", angle);
}

/**
 * Move the servo smooth
*/
void moveJointAbsolute(int angleMega, int angleElle, float duration) {
  // b is command
  // m is megaPos, e is ellePos, d is duration
  currentAngleJointLVL1 = angleMega;
  currentAngleJointLVL2 = angleElle;
  servoSerial.printf("!!bm%de%dd%f\n", angleMega, angleElle, duration);
  wait_ms( duration > 0 ? 1200 : duration);
}

void moveJointAbsoluteNoWait(int angleMega, int angleElle, float duration) {
  // b is command
  // m is megaPos, e is ellePos, d is duration
  currentAngleJointLVL1 = angleMega;
  currentAngleJointLVL2 = angleElle;
  servoSerial.printf("!!bm%de%dd%f\n", angleMega, angleElle, duration);
}

void activateOthers(int team) {
  servoSerial.printf("!!a%d\n", team);
}

void pushIn(double dist, bool adjustForOffSet, bool invert){  //distance facing in the y in mm | adjustForOffSet true = makes the global position of the arm from lvl 2+ stay at the same point on the local x axis (the arm moves straight)(robot drives)   adjustForOffSet = false only the arm moves to the desired point (robot doesn't drive)
  double correctedDist = dist-jointLVL2Length-jointLVL3Length;//calcualte mathamatically
  double radiansAlapha = asin(correctedDist/jointLVL1Length);
  double degreesAlapha = (radiansAlapha*180.0/pi);
  double offSetDist = sqrt(sq(jointLVL1Length)-sq(correctedDist));

  int jointLVL1Angle;
  int jointLVL2Angle;
  if (invert) {
    jointLVL1Angle = -degreesAlapha;//translate to relevant instructions
    jointLVL2Angle = 90 + degreesAlapha;
    offSetDist = -offSetDist;
  } else {
    jointLVL1Angle = degreesAlapha;//translate to relevant instructions
    jointLVL2Angle = (90-degreesAlapha);
  }

  printf("LVL1 to %d deg | LVL2 to %d deg | Offset: %fmm\n", jointLVL1Angle, jointLVL2Angle, offSetDist);

  //execute instructions
  if(adjustForOffSet == true){
    long totalMovementTime = (int) startSlowDrive(abs(offSetDist), -offSetDist > 0);
    moveJointAbsolute(jointLVL1Angle, jointLVL2Angle, totalMovementTime);
  } else{
    moveJointAbsolute(jointLVL1Angle, jointLVL2Angle, -1);
  }
}

void pushIn(double dist, bool adjustForOffSet){//distance facing in the y in mm | adjustForOffSet true = makes the global position of the arm from lvl 2+ stay at the same point on the local x axis (the arm moves straight)(robot drives)   adjustForOffSet = false only the arm moves to the desired point (robot doesn't drive)
  double correctedDist = dist-jointLVL2Length-jointLVL3Length;//calcualte mathamatically
  double radiansAlapha = asin(correctedDist/jointLVL1Length);
  double degreesAlapha = (radiansAlapha*180.0/pi);
  double offSetDist = sqrt(sq(jointLVL1Length)-sq(correctedDist));

  int jointLVL1Angle = degreesAlapha;     //translate to relevant instructions
  int jointLVL2Angle = (90-degreesAlapha);

  printf("LVL1 to %d deg | LVL2 to %d deg | Offset: %fmm\n", jointLVL1Angle, jointLVL2Angle, offSetDist);

  //execute instructions
  if(adjustForOffSet == true){
    long totalMovementTime = (int) startSlowDrive(abs(lastArmXOffSet-offSetDist), lastArmXOffSet-offSetDist > 0);
    moveJointAbsolute(jointLVL1Angle, jointLVL2Angle, totalMovementTime);
  } else{
    moveJointAbsolute(jointLVL1Angle, jointLVL2Angle, 600);
  }
}


void goToPoint(double x, double y, bool scanMode){//scanMode false = refrence is suction cup, scanMode true = refrence is sensor | x and y in relation to the rotational point of joint 1 with +x beeing the forward facing direction of the robot and +y the 90° rotation to the left from that when looking in forward driving direction (in mm)
  double modeBasedJointLVL2Length = 0;
  int modeBasedJointLVL2AngleOffset = 0;
  if(scanMode == true){
    modeBasedJointLVL2Length = jointLVL2LengthSensor;
    modeBasedJointLVL2AngleOffset = jointLVL2ToSensorAngleOffset;
  } else{
    modeBasedJointLVL2Length = jointLVL2LengthScutionCup;
    modeBasedJointLVL2AngleOffset = jointLVL2ToSuctionCupAngleOffset;
  }
  
  double hypotenuseISCH = sqrt(sq(x)+sq(y));//calcualte mathamatically
  double radiansAlapha1 = acos((sq(modeBasedJointLVL2Length)-sq(jointLVL1Length)-sq(hypotenuseISCH))/(-2.0*jointLVL1Length*hypotenuseISCH));
  double radiansGamma1 = acos((sq(hypotenuseISCH)-sq(modeBasedJointLVL2Length)-sq(jointLVL1Length))/(-2.0*modeBasedJointLVL2Length*jointLVL1Length));
  double radiansAlapha2 = atan(y/x);
  double degreesAlapha1 = (radiansAlapha1*180.0/pi);
  double degreesGamma1 = (radiansGamma1*180.0/pi);
  double degreesAlapha2 = (radiansAlapha2*180.0/pi);
  if(x < 0){
    degreesAlapha2 = 180+degreesAlapha2;
  }

  int jointLVL1Angle = degreesAlapha1+degreesAlapha2;//translate to relevant instructions
  int jointLVL2Angle = degreesGamma1-180+modeBasedJointLVL2AngleOffset;

  moveJointAbsolute(jointLVL1Angle, jointLVL2Angle, 600);//execute instructions
}