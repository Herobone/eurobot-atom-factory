#include <Arduino.h>
#include <Servo.h>
#include <SoftwareSerial.h>
#include <NRFLite.h>

#pragma region Defines

Servo jointLVL1;
Servo jointLVL2;
Servo jointLVL3;

#define megaPin 7
#define ellePin 8
#define tiltPin 5

//Those are the values to start with
int megaPos = 180;
int ellePos = 135;
int tiltPos = 90;

//runtime variables
int currentAngleJointLVL1 = 0;
int currentAngleJointLVL2 = 0; 
int currentAngleJointLVL3 = 0;

const int servo0ToRobot0JointLVL1 = 165;//This is when the servo is facing forwards in the driving direction
const int servo0ToRobot0JointLVL2 = 93;//This is when the servo is facing straight ahead in the same direction as the Joint1 is pointing
const int jointLVL1MaxAngle = 170;//This is when the servos maximum safe angle
const int jointLVL2MaxAngle = 179;//This is when the servos maximum safe angle
const int jointLVL3MaxAngle = 179;//This is when the servos maximum safe angle
const int jointLVL3UpwardAngle = 13;//This is when the servos angle when facing upward
const int jointLVL3DownwardAngle = 120;//This is when the servos angle when facing downward
const double degreesPerTick = 1.5;//USED IN moveJointLVL3Up||moveJoint
const double msBetweenTick = 20.0;//USED IN moveJointLVL3Up||moveJoint
bool jointLVL3FacingUp = false;

int activate = NULL;

String inputString = "";
bool stringComplete = false;

String inputStringDeb = "";
bool stringCompleteDeb = false;

SoftwareSerial debug(2, 3); //RX, TX connection to the debug nano

const static uint8_t RADIO_ID = 38;                   // ID of Big Bot
const static uint8_t DESTINATION_RADIO_ID_PANEL = 104; // ID of panel
const static uint8_t DESTINATION_RADIO_ID_LILB = 86;  // ID of little bot
const static uint8_t PIN_RADIO_CE = 9;
const static uint8_t PIN_RADIO_CSN = 10;

bool PANEL_OK = false;
bool LILB_OK = false;

#define println(text) {Serial.println(text); debug.println(text);}
#define print(text) {Serial.print(text); debug.print(text);}

struct RadioPacket // Any packet up to 32 bytes can be sent.
{
  uint8_t FromRadioId;
  uint8_t TeamID;
  uint16_t FailedTxCount;
};

NRFLite radio;
RadioPacket radioData_PANEL;
RadioPacket radioData_LILB;

const double jointLVL1Length = 80.0;
const double jointLVL2Length = 44.5;
const double jointLVL2LengthScutionCup = 47.0;
const double jointLVL2LengthSensor = 58.5;
const double jointLVL3Length = 88.5;
const int jointLVL2ToSuctionCupAngleOffset = 19;//in refrence to servo0ToRobot0JointLVL2
const int jointLVL2ToSensorAngleOffset = -41;//in refrence to servo0ToRobot0JointLVL2
const int zAxisLimitSwitchGroundOffset = 22;//This is the distance from the suction-cup to the ground (jointLVL3FacingUp = false) when the zAxisLimitSwitch is barrely activated in mm     !!!!!!!!!!!!!!!!NEEDS ADJUSTMENT!!!!!!!!!!!!!!!!
const int zAxisMaxGroundOffset = 145;//This is the max distance from the suction-cup to the ground (jointLVL3FacingUp = false) in mm     !!!!!!!!!!!!!!!!NEEDS ADJUSTMENT!!!!!!!!!!!!!!!!

#pragma endregion

void moveJointAbsolute(int jointLVL1Angle, int jointLVL2Angle, long timeMS);

void goToPoint(double x, double y, bool scanMode){//scanMode false = refrence is suction cup, scanMode true = refrence is sensor | x and y in relation to the rotational point of joint 1 with +x beeing the forward facing direction of the robot and +y the 90° rotation to the left from that when looking in forward driving direction (in mm)
  double modeBasedJointLVL2Length = 0;
  int modeBasedJointLVL2AngleOffset = 0;
  if(scanMode == true){
    modeBasedJointLVL2Length = jointLVL2LengthSensor;
    modeBasedJointLVL2AngleOffset = jointLVL2ToSensorAngleOffset;
  } else{
    modeBasedJointLVL2Length = jointLVL2LengthScutionCup;
    modeBasedJointLVL2AngleOffset = jointLVL2ToSuctionCupAngleOffset;
  }
  
  double hypotenuseISCH = sqrt(sq(x)+sq(y));//calcualte mathamatically
  double radiansAlapha1 = acos((sq(modeBasedJointLVL2Length)-sq(jointLVL1Length)-sq(hypotenuseISCH))/(-2.0*jointLVL1Length*hypotenuseISCH));
  double radiansGamma1 = acos((sq(hypotenuseISCH)-sq(modeBasedJointLVL2Length)-sq(jointLVL1Length))/(-2.0*modeBasedJointLVL2Length*jointLVL1Length));
  double radiansAlapha2 = atan(y/x);
  double degreesAlapha1 = (radiansAlapha1*180.0/PI);
  double degreesGamma1 = (radiansGamma1*180.0/PI);
  double degreesAlapha2 = (radiansAlapha2*180.0/PI);
  if(x < 0){
    degreesAlapha2 = 180+degreesAlapha2;
  }

  int jointLVL1Angle = degreesAlapha1+degreesAlapha2;//translate to relevant instructions
  int jointLVL2Angle = degreesGamma1-180+modeBasedJointLVL2AngleOffset;

  print("LVL1: ");
  print(jointLVL1Angle);
  print(" LVL2: ");
  println(degreesGamma1);


  moveJointAbsolute(jointLVL1Angle, jointLVL2Angle, 600);//execute instructions
}

void serialEvent()
{
  while (Serial.available())
  {
    char inChar = (char)Serial.read();
    //debug.write(inChar);
    if (inChar == '\n')
    {
      stringComplete = true;
    }
    else
    {
      inputString += inChar;
    }
  }
}

void moveJointLVL1(int angle){
  int newAngle = max(0, angle);
  newAngle = min(newAngle, jointLVL1MaxAngle);
  jointLVL1.write(newAngle);
  currentAngleJointLVL1 = newAngle;
  if(angle != newAngle){
    println("Impossible demand angle at JointLVL1");
  }
}
void moveJointLVL2(int angle){
  int newAngle = max(0, angle);
  newAngle = min(newAngle, jointLVL2MaxAngle);
  jointLVL2.write(newAngle);
  currentAngleJointLVL2 = newAngle;
  if(angle != newAngle){
    println("Impossible demand angle at JointLVL2");
  }
}
void moveJointLVL3(int angle){
  int newAngle = max(0, angle);
  newAngle = min(newAngle, jointLVL3MaxAngle);
  jointLVL3.write(newAngle);
  currentAngleJointLVL3 = newAngle;
  if(angle != newAngle){
    println("Impossible demand angle at JointLVL3");
  }
}

void initServos(){
  jointLVL1.attach(megaPin);
  jointLVL2.attach(ellePin);
  jointLVL3.attach(tiltPin);
  moveJointLVL1(0);
  moveJointLVL2(0);
  moveJointLVL3(0);
}

void moveJointLVL3Up(bool state){//moves the 3rd Joint in the upward position for true and in the downward position for false
  int angleTo = 0;
  if(state){angleTo = jointLVL3UpwardAngle;}
  else{angleTo = jointLVL3DownwardAngle;}
  int distanceJointLVL3Angle = angleTo-currentAngleJointLVL3;
  int stepNumber = (((double)distanceJointLVL3Angle)/degreesPerTick)+1.0;
  int timeLeft = stepNumber*msBetweenTick;
  for(int i = 0; i < stepNumber; i++){
    moveJointLVL3(currentAngleJointLVL3+(int)(((double)distanceJointLVL3Angle)/((double)(stepNumber-i))));
    delay(((double)timeLeft)/((double)(stepNumber-i)));
    distanceJointLVL3Angle = angleTo-currentAngleJointLVL3;
    timeLeft = timeLeft-((double)timeLeft)/((double)(stepNumber-i));
  }
  jointLVL3FacingUp = state;
  moveJointLVL3(angleTo);
}

void moveJoint(int jointLVL1Angle, int jointLVL2Angle, long timeMS){//moves the servo to the angle angle (angle is not in relation to the robots 0 but to the servos 0 so the conversion needes to be handled beforehand) in the time timeMS
  int distanceJointLVL1Angle = jointLVL1Angle-currentAngleJointLVL1;
  int distanceJointLVL2Angle = jointLVL2Angle-currentAngleJointLVL2;
  int stepNumber = (((double)timeMS)/msBetweenTick)+1.0;
  int timeLeft = timeMS;
  if(timeMS <= 0){//if time <=0 an established standard is used in order to save time and ensure a consistent speed
    stepNumber = (((double)max(abs(distanceJointLVL1Angle), abs(distanceJointLVL2Angle)))/degreesPerTick)+1.0;
    timeLeft = stepNumber*msBetweenTick;
  }
  for(int i = 0; i < stepNumber; i++){
    moveJointLVL1(currentAngleJointLVL1+(int)(((double)distanceJointLVL1Angle)/((double)(stepNumber-i))));
    moveJointLVL2(currentAngleJointLVL2+(int)(((double)distanceJointLVL2Angle)/((double)(stepNumber-i))));
    delay(((double)timeLeft)/((double)(stepNumber-i)));
    distanceJointLVL1Angle = jointLVL1Angle-currentAngleJointLVL1;
    distanceJointLVL2Angle = jointLVL2Angle-currentAngleJointLVL2;
    timeLeft = timeLeft-((double)timeLeft)/((double)(stepNumber-i));
  }
  moveJointLVL1(jointLVL1Angle);
  moveJointLVL2(jointLVL2Angle);
}

void moveJointAbsolute(int jointLVL1Angle, int jointLVL2Angle, long timeMS){//moves the servo to the angle absoluteAngle (angle is in relation to the robots 0 and not to the servos 0 so the conversion has already been handled) in the time timeMS 
  moveJoint(servo0ToRobot0JointLVL1-jointLVL1Angle, servo0ToRobot0JointLVL2-jointLVL2Angle, timeMS);
}

void setup()
{
  Serial.begin(115200);
  debug.begin(115200);
  println("Armuino online");
  inputString.reserve(200);
  initServos();
  if (!radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN))
  {
    println("Cannot communicate with radio");
    while (1)
    {
      delay(1000);
    }
  }

  radioData_LILB.FromRadioId = RADIO_ID;
  radioData_PANEL.FromRadioId = RADIO_ID;
}

void loop()
{
  if (stringComplete)
  {
    println(inputString);
    String substr = inputString.substring(inputString.indexOf("!!") + 2);
    if (substr != NULL)
    {
      //println(substr);
      char servoAddr = substr.charAt(0);
      if (servoAddr == 'm')       // Set angle for Mega
      {
        moveJointLVL1(substr.substring(substr.indexOf('m') + 1).toInt());
      }
      else if (servoAddr == 't')  // Set angle for Tilt
      {
        moveJointLVL3Up(substr.substring(substr.indexOf('t') + 1).toInt() == 1);
      }
      else if (servoAddr == 'e')  // Set angle for Elle
      {
        moveJointLVL2(substr.substring(substr.indexOf('e') + 1).toInt());
      }
      else if (servoAddr == 'a')  // Activate
      {
        activate = substr.substring(substr.indexOf('a') + 1).toInt();
        println("Some kind of activation stuff goin on!");
      }
      else if (servoAddr == 'b')  // Smooth just mega and elle
      {
        moveJointAbsolute(substr.substring(substr.indexOf('m') + 1).toInt(), substr.substring(substr.indexOf('e') + 1).toInt(), substr.substring(substr.indexOf('d') + 1).toFloat());
      }
      else if (servoAddr == 'p')  // Smooth just mega and elle
      {
        goToPoint(substr.substring(substr.indexOf('x') + 1).toDouble(), substr.substring(substr.indexOf('y') + 1).toDouble(), substr.substring(substr.indexOf('s') + 1).toInt() == 1);
      }
    }
    stringComplete = false;
    inputString = "";
  }

  #pragma region DEBUG
  while (debug.available())
  {
    char inChar = (char)debug.read();
    if (inChar == '\n')
    {
      stringCompleteDeb = true;
    }
    else
    {
      inputStringDeb += inChar;
    }
  }

  if (stringCompleteDeb)
  {
    String substr = inputStringDeb.substring(inputStringDeb.indexOf("!!ARMUINO!!") + 11);
    if (substr != NULL)
    {
      char servoAddr = substr.charAt(0);
      if (servoAddr == 'm')
      {
        megaPos = substr.substring(substr.indexOf('m') + 1).toInt();
        print("Mega to ");
        println(megaPos);
      }
      else if (servoAddr == 't')
      {
        tiltPos = substr.substring(substr.indexOf('t') + 1).toInt();
        print("Tilt to ");
        println(tiltPos);
      }
      else if (servoAddr == 'e')
      {
        ellePos = substr.substring(substr.indexOf('e') + 1).toInt();
      }
      else if (servoAddr == 'a')
      {
        activate = substr.substring(substr.indexOf('a') + 1).toInt();
        println("Some kind of activation stuff goin on!");
      }
    }
    stringCompleteDeb = false;
    inputStringDeb = "";
  }
  #pragma endregion

  #pragma region RF_Activation
  if (activate != NULL)
  {
    print("Sending ");
    print(activate);
    println("...");

    radioData_LILB.TeamID = activate;
    radioData_PANEL.TeamID = activate;

    if (!LILB_OK)
    {
      if (radio.send(DESTINATION_RADIO_ID_LILB, &radioData_LILB, sizeof(radioData_LILB)))
      {
        println("... Success for Little Bot");
        LILB_OK = true;
      }
      else
      {
        println("... Failed for Little Bot");
        radioData_LILB.FailedTxCount++;
        if (radioData_LILB.FailedTxCount > 15) {
          LILB_OK = true;
        }
      }
    }

    if (!PANEL_OK)
    {
      if (radio.send(DESTINATION_RADIO_ID_PANEL, &radioData_PANEL, sizeof(radioData_PANEL)))
      {
        println("... Success for Panel");
        PANEL_OK = true;
      }
      else
      {
        println("... Failed for Panel");
        radioData_PANEL.FailedTxCount++;
        if (radioData_PANEL.FailedTxCount > 15) {
          PANEL_OK = true;
        }
      }
    }

    if (LILB_OK && PANEL_OK)
    {
      activate = NULL;
      LILB_OK = false;
      PANEL_OK = false;
    }
  }
  #pragma endregion
}
