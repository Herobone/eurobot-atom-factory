#include <Arduino.h>
#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>

#define THERMISTORPIN A0
#define THERMISTORNOMINAL 100000
// temp. for nominal resistance (almost always 25 C)
#define TEMPERATURENOMINAL 25
#define NUMSAMPLES 5
// The beta coefficient of the thermistor (usually 3000-4000)
#define BCOEFFICIENT 3950
#define SERIESRESISTOR 10000

int samples[NUMSAMPLES];

File myFile;

const int chipSelect = 10;
String filename = "test";
String fileType = ".log";

String fromBLE = "";
String fromNucleo = "";
String fromArm = "";
bool bleDone = false;
bool nucleoDone = false;
bool armDone = false;

char *logFileName = "errorWhileInit.log";

//SoftwareSerial nucleo(2, 3); //RX, TX connection to the nucleo
SoftwareSerial armuino(4, 5); //RX, TX connection to the armuino
SoftwareSerial ble(2, 3);

void outln(String text)
{
  ble.println(text);
  myFile.println(text);
}

void out(String text)
{
  ble.print(text);
  myFile.print(text);
}

void setup()
{
  ble.begin(115200);      //Connection to BLE
  Serial.begin(115200);  //Connection to NUCLEO
  armuino.begin(115200); //Connection to ARMUINO

  ble.print("Initializing SD card...");
  pinMode(SS, OUTPUT);

  if (!SD.begin(chipSelect))
  {
    ble.println("initialization failed!");
    return;
  }
  ble.println("initialization done.");

  String tempFileName = filename + fileType;
  tempFileName.toCharArray(logFileName, tempFileName.length());
  int enumeration = 0;
  /*ble.print("Trying: (String) ");
  ble.print(tempFileName);
  ble.print(" OR (char*) ");
  ble.print(logFileName);
  ble.print(" lenght: ");
  ble.println(tempFileName.length());*/
  delay(100);
  while (SD.exists(logFileName))
  {
    //ble.println("Does exist!");
    //ble.println();
    enumeration++;
    tempFileName = filename + enumeration + fileType;
    tempFileName.toCharArray(logFileName, tempFileName.length() + 1);

    /*ble.print("Trying: (String) ");
    ble.print(tempFileName);
    ble.print(" OR (char*) ");
    ble.print(logFileName);
    ble.print(" lenght: ");
    ble.println(tempFileName.length());
    delay(100);*/
  }

  ble.print("Name chosen: ");
  ble.println(logFileName);

  myFile = SD.open(logFileName, FILE_WRITE);
  outln("SD initialized!");

  analogReference(EXTERNAL);
  outln("Init done!");
  out("Uploaded: ");   outln(__DATE__);
  outln("");

  Serial.println("START");
  armuino.listen();
}

float readTemp()
{
  uint8_t i;
  float average;

  // take N samples in a row, with a slight delay
  for (i = 0; i < NUMSAMPLES; i++)
  {
    samples[i] = analogRead(THERMISTORPIN);
    delay(10);
  }

  // average all the samples outln
  average = 0;
  for (i = 0; i < NUMSAMPLES; i++)
  {
    average += samples[i];
  }
  average /= NUMSAMPLES;

  ble.print("Average analog reading ");
  ble.println(average);
  // convert the value to resistance
  average = 1023 / average - 1;
  average = SERIESRESISTOR / average;

  float steinhart;
  steinhart = average / THERMISTORNOMINAL;          // (R/Ro)
  steinhart = log(steinhart);                       // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                        // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  steinhart = 1.0 / steinhart;                      // Invert
  steinhart -= 273.15;                              // convert to C

  return steinhart;
}

void serialEvent()
{
  while (Serial.available())
  {
    char fromNucleoSerial = (char)Serial.read();
    fromNucleo += fromNucleoSerial;

    if (fromNucleoSerial == '\n')
    {
      nucleoDone = true;
    }
  }
}

void loop()
{
  if (nucleoDone)
  {
    ble.print(fromNucleo);
    myFile.print(fromNucleo);

    if (fromNucleo.equalsIgnoreCase("!END LOGGING!\n"))
    {
      outln("Logging was interrupted!");
      outln("Saving file!");
      myFile.close();
    }

    fromNucleo = "";
    nucleoDone = false;
  }

  /*while (ble.available())
  {
    char fromBLESerial = (char)ble.read();
    fromBLE += fromBLESerial;

    if (fromBLESerial == '\n')
    {
      bleDone = true;
    }
  }*/

  while (armuino.available())
  {
    char fromArmSerial = (char)armuino.read();
    fromArm += fromArmSerial;

    if (fromArmSerial == '\n')
    {
      armDone = true;
    }
  }

  if (armDone) {
    ble.print(fromArm);
    myFile.print(fromArm);
    fromArm = "";
    armDone = false;
  }
  

  /*if (bleDone)
  {
    ble.print("[BLE]" + fromBLE);
    Serial.print(fromBLE);
    armuino.print(fromBLE);
    if (fromBLE.equalsIgnoreCase("!END LOGGING!\n"))
    {
      outln("Logging was interrupted!");
      outln("Saving file!");
      myFile.close();
    }

    if (fromBLE.equalsIgnoreCase("!TEMPS!\n"))
    {
      outln("Temperature reading!");
      float temp = readTemp();
      outln("Temp is: " + String(temp) + " °C");
    }

    myFile.print("[BLE]" + fromBLE);

    fromBLE = "";
    bleDone = false;
  }*/
}
